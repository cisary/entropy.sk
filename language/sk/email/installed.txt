Subject: phpBB nainštalované

Gratulujeme,

Inštalácia phpBB na Váš server prebehla úspešne.

Tento email obsahuje dôležité informácie týkajúce sa Vašej inštalácie. Mali by ste si ich bezpečne uschovať. Heslo bolo v databáze zašifrované a neexistuje možnosť, ako ho zistiť. V prípade, že zabudnete Vaše heslo, môžete požiadať o nové.

----------------------------
Užívateľ: {USERNAME}


URL fóra: {U_BOARD}
----------------------------

Za účelom bezpečnosti fóra, odporúčame držať krok s vývojom softvéru, čo ľahko dosiahnete prihlásením sa na prijímanie emailu týkajuceho sa phpBB.
Prihlásiť sa môťete na - https://www.phpbb.com/support/

V prípade záujmu o vytvorenie nového vzhľadu, modifikácie alebo akejkoľvek úpravy phpBB systému, sa môžete obrátiť na http://phpbb3hacks.com/phpbb_services.php?lang=sk

{EMAIL_SIG}
