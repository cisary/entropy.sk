<?php if (!defined('IN_PHPBB')) exit; ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="<?php echo (isset($this->_rootref['S_CONTENT_DIRECTION'])) ? $this->_rootref['S_CONTENT_DIRECTION'] : ''; ?>" lang="<?php echo (isset($this->_rootref['S_USER_LANG'])) ? $this->_rootref['S_USER_LANG'] : ''; ?>" xml:lang="<?php echo (isset($this->_rootref['S_USER_LANG'])) ? $this->_rootref['S_USER_LANG'] : ''; ?>">
<head>
<meta charset="utf-8" />
<meta http-equiv="content-type" content="text/html; charset=<?php echo (isset($this->_rootref['S_CONTENT_ENCODING'])) ? $this->_rootref['S_CONTENT_ENCODING'] : ''; ?>" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-language" content="<?php echo (isset($this->_rootref['S_USER_LANG'])) ? $this->_rootref['S_USER_LANG'] : ''; ?>" />
<meta http-equiv="imagetoolbar" content="no" />
<meta name="resource-type" content="document" />
<meta name="distribution" content="global" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php echo (isset($this->_rootref['META'])) ? $this->_rootref['META'] : ''; ?>

<title>entropy.sk &bull; <?php if ($this->_rootref['S_IN_MCP']) {  echo ((isset($this->_rootref['L_MCP'])) ? $this->_rootref['L_MCP'] : ((isset($user->lang['MCP'])) ? $user->lang['MCP'] : '{ MCP }')); ?> &bull; <?php } else if ($this->_rootref['S_IN_UCP']) {  echo ((isset($this->_rootref['L_UCP'])) ? $this->_rootref['L_UCP'] : ((isset($user->lang['UCP'])) ? $user->lang['UCP'] : '{ UCP }')); ?> &bull; <?php } echo (isset($this->_rootref['PAGE_TITLE'])) ? $this->_rootref['PAGE_TITLE'] : ''; ?></title>

<?php if ($this->_rootref['S_ENABLE_FEEDS']) {  if ($this->_rootref['S_ENABLE_FEEDS_OVERALL']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo (isset($this->_rootref['SITENAME'])) ? $this->_rootref['SITENAME'] : ''; ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_NEWS']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FEED_NEWS'])) ? $this->_rootref['L_FEED_NEWS'] : ((isset($user->lang['FEED_NEWS'])) ? $user->lang['FEED_NEWS'] : '{ FEED_NEWS }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=news" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_FORUMS']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_ALL_FORUMS'])) ? $this->_rootref['L_ALL_FORUMS'] : ((isset($user->lang['ALL_FORUMS'])) ? $user->lang['ALL_FORUMS'] : '{ ALL_FORUMS }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=forums" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_TOPICS']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FEED_TOPICS_NEW'])) ? $this->_rootref['L_FEED_TOPICS_NEW'] : ((isset($user->lang['FEED_TOPICS_NEW'])) ? $user->lang['FEED_TOPICS_NEW'] : '{ FEED_TOPICS_NEW }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=topics" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_TOPICS_ACTIVE']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FEED_TOPICS_ACTIVE'])) ? $this->_rootref['L_FEED_TOPICS_ACTIVE'] : ((isset($user->lang['FEED_TOPICS_ACTIVE'])) ? $user->lang['FEED_TOPICS_ACTIVE'] : '{ FEED_TOPICS_ACTIVE }')); ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?mode=topics_active" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_FORUM'] && $this->_rootref['S_FORUM_ID']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_FORUM'])) ? $this->_rootref['L_FORUM'] : ((isset($user->lang['FORUM'])) ? $user->lang['FORUM'] : '{ FORUM }')); ?> - <?php echo (isset($this->_rootref['FORUM_NAME'])) ? $this->_rootref['FORUM_NAME'] : ''; ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?f=<?php echo (isset($this->_rootref['S_FORUM_ID'])) ? $this->_rootref['S_FORUM_ID'] : ''; ?>" /><?php } if ($this->_rootref['S_ENABLE_FEEDS_TOPIC'] && $this->_rootref['S_TOPIC_ID']) {  ?><link rel="alternate" type="application/atom+xml" title="<?php echo ((isset($this->_rootref['L_FEED'])) ? $this->_rootref['L_FEED'] : ((isset($user->lang['FEED'])) ? $user->lang['FEED'] : '{ FEED }')); ?> - <?php echo ((isset($this->_rootref['L_TOPIC'])) ? $this->_rootref['L_TOPIC'] : ((isset($user->lang['TOPIC'])) ? $user->lang['TOPIC'] : '{ TOPIC }')); ?> - <?php echo (isset($this->_rootref['TOPIC_TITLE'])) ? $this->_rootref['TOPIC_TITLE'] : ''; ?>" href="<?php echo (isset($this->_rootref['U_FEED'])) ? $this->_rootref['U_FEED'] : ''; ?>?f=<?php echo (isset($this->_rootref['S_FORUM_ID'])) ? $this->_rootref['S_FORUM_ID'] : ''; ?>&amp;t=<?php echo (isset($this->_rootref['S_TOPIC_ID'])) ? $this->_rootref['S_TOPIC_ID'] : ''; ?>" /><?php } } ?>


<!--
	phpBB style name: Lucid Lime
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:	  Eric Seguin
-->

<script type="text/javascript">
// <![CDATA[
	var jump_page = '<?php echo ((isset($this->_rootref['LA_JUMP_PAGE'])) ? $this->_rootref['LA_JUMP_PAGE'] : ((isset($this->_rootref['L_JUMP_PAGE'])) ? addslashes($this->_rootref['L_JUMP_PAGE']) : ((isset($user->lang['JUMP_PAGE'])) ? addslashes($user->lang['JUMP_PAGE']) : '{ JUMP_PAGE }'))); ?>:';
	var on_page = '<?php echo (isset($this->_rootref['ON_PAGE'])) ? $this->_rootref['ON_PAGE'] : ''; ?>';
	var per_page = '<?php echo (isset($this->_rootref['PER_PAGE'])) ? $this->_rootref['PER_PAGE'] : ''; ?>';
	var base_url = '<?php echo (isset($this->_rootref['A_BASE_URL'])) ? $this->_rootref['A_BASE_URL'] : ''; ?>';
	var style_cookie = 'phpBBstyle';
	var style_cookie_settings = '<?php echo (isset($this->_rootref['A_COOKIE_SETTINGS'])) ? $this->_rootref['A_COOKIE_SETTINGS'] : ''; ?>';
	var onload_functions = new Array();
	var onunload_functions = new Array();

	<?php if ($this->_rootref['S_USER_PM_POPUP'] && $this->_rootref['S_NEW_PM']) {  ?>

		var url = '<?php echo (isset($this->_rootref['UA_POPUP_PM'])) ? $this->_rootref['UA_POPUP_PM'] : ''; ?>';
		window.open(url.replace(/&amp;/g, '&'), '_phpbbprivmsg', 'height=225,resizable=yes,scrollbars=yes, width=400');
	<?php } ?>


	/**
	* Find a member
	*/
	function find_username(url)
	{
		popup(url, 760, 570, '_usersearch');
		return false;
	}

	/**
	* New function for handling multiple calls to window.onload and window.unload by pentapenguin
	*/
	window.onload = function()
	{
		for (var i = 0; i < onload_functions.length; i++)
		{
			eval(onload_functions[i]);
		}
	};

	window.onunload = function()
	{
		for (var i = 0; i < onunload_functions.length; i++)
		{
			eval(onunload_functions[i]);
		}
	};
	
	function twitterhover() {
    	document.getElementById("twitter-img").src = "http://entropy.sk/my/twitter2.png";
  }
  
  function twitterendhover() {
    	document.getElementById("twitter-img").src = "http://entropy.sk/my/twitter_h.png";
  }
  
  function twitter2hover() {
    	document.getElementById("twitter2-img").src = "http://entropy.sk/my/twitter2.png";
  }
  
  function twitter2endhover() {
    	document.getElementById("twitter2-img").src = "http://entropy.sk/my/twitter.png";
  }
  
  	function trellohover() {
    	document.getElementById("trello-img").src = "http://entropy.sk/my/trello.png";
  }
  
  function trelloendhover() {
    	document.getElementById("trello-img").src = "http://entropy.sk/my/trello_h.png";
  }
  
  function logohover() {
    	document.getElementById("logo-img").src = "http://entropy.sk/my/site_logo_h.png";
  }
  
  function logoendhover() {
    	document.getElementById("logo-img").src = "./styles/lucid_lime/imageset/site_logo.png";
  }
  
  function menuhover() {
    	document.getElementById("menu-img").src = "./styles/lucid_lime/imageset/seed_menu.png";
  }
  
  function menuendhover() {
    	document.getElementById("menu-img").src = "./styles/lucid_lime/imageset/seed_menu2.png";
  }

// ]]>
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script>

 
/*
  
  
var path="http://entropy.sk/styles/lucid_lime/imageset";

var preload_pictures    = function(picture_urls, callback)
{
    var loaded  = 0;

    for(var i = 0, j = picture_urls.length; i < j; i++)
    {
        var img     = new Image();

        img.onload  = function()
        {                               
            if(++loaded == picture_urls.length && callback)
            {
                callback();
            }
        }

        img.src = picture_urls[i];
    }
};

   
    $(document).ready(function() {
        
        preload_pictures(
        
        [
        path+'/background.png',path+'/foreground.png',path+'/site_logo.png',path+'/notwholestromgrey.jpg',path+'/seed_back.jpg',path+'/seed_menu.png',path+'/seed_menu2.png',path+'/wholestromgreycomini.jpg',
        
        ], function(){
        
        $("#progress").fadeOut(5000,function() {
            $(".afterloaded").fadeIn(1000);
        });
    });
        
   /* 
    delay(800).queue(function() {
		
		  
		  
		  //$form_inputs.first().queue(function() {
			$(this).focus();
		 // });
    
    
        setTimeout(function() { 
        
        $("#background").fadeIn(1700, function() {
        $("#midground").fadeIn(1000, function() {
        $("#foreground").fadeIn(1400);
        /*
        setTimeout(function() { 
        
        $("#background").fadeOut(15000, function() {
        $("#midground").fadeOut(16000, function() {
        $("#foreground").fadeOut(17000);
        
        });
        });
        
        },1000);*/
        /*
        });
        });
        
        },300);
        
              
        
});
        
        */
        
  

	</script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['T_SUPER_TEMPLATE_PATH'])) ? $this->_rootref['T_SUPER_TEMPLATE_PATH'] : ''; ?>/styleswitcher.js"></script>
<script type="text/javascript" src="<?php echo (isset($this->_rootref['T_SUPER_TEMPLATE_PATH'])) ? $this->_rootref['T_SUPER_TEMPLATE_PATH'] : ''; ?>/forum_fn.js"></script>
<script src="//platform.twitter.com/widgets.js" type="text/javascript"></script>
<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/print.css" rel="stylesheet" type="text/css" media="print" title="printonly" />
<link href="<?php echo (isset($this->_rootref['T_STYLESHEET_LINK'])) ? $this->_rootref['T_STYLESHEET_LINK'] : ''; ?>" rel="stylesheet" type="text/css" media="screen, projection" />

<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/normal.css" rel="stylesheet" type="text/css" title="A" />
<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/medium.css" rel="alternate stylesheet" type="text/css" title="A+" />
<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/large.css" rel="alternate stylesheet" type="text/css" title="A++" />

<?php if ($this->_rootref['S_CONTENT_DIRECTION'] == ('rtl')) {  ?>

	<link href="<?php echo (isset($this->_rootref['T_THEME_PATH'])) ? $this->_rootref['T_THEME_PATH'] : ''; ?>/bidi.css" rel="stylesheet" type="text/css" media="screen, projection" />
<?php } ?>


<style>
/* 
    Starry Night DEMO
    chriscoyier@gmail.com
    http://chriscoyier.net
*/

* { margin: 0; padding: 0; font-family: 'Tahoma';}

html { overflow-y: scroll; font-size: 100%;
	/* Always show a scrollbar for short pages - stops the jump when the scrollbar appears. non-IE browsers */
	height: 101%; }

.clear {
	clear: both;
}

ul#progress {
    list-style: none;
    width: 125px;
    margin: 0 auto;
    padding-top: 50px;
    padding-bottom: 50px;
}

ul#progress li {
    /* Style your list */
	float: left;
    position: relative;
    width: 15px;
    height: 15px;
    border: 1px solid #fff;
    border-radius: 50px;
    margin-left: 10px;
    border-left: 1px solid #111;
    border-top: 1px solid #111;
    border-right: 1px solid #333;
    border-bottom: 1px solid #333;
    background: #ffffff;
}

ul#progress li:first-child {
    margin-left: 0;
} /* Remove the margin first li element */

.ball {
    /* Style your ball and set the animation */
	background-color: #349aff;
    background-image: -moz-linear-gradient(90deg, #349aff 100%, #ffffff 25%);
    background-image: -webkit-linear-gradient(90deg, #349aff 25%, #ffffff 100%);
    width: 15px;
    height: 15px;
    border-radius: 50px;
    -moz-transform: scale(0);
    -webkit-transform: scale(0);
    -moz-animation: loading 1s linear forwards;
    -webkit-animation: loading 1s linear forwards;
}

.afterloaded {
    display: none;
}

.pulse {
    /* Style your second ball to create the amazing effects */
	width: 15px;
    height: 15px;
    border-radius: 30px;
    border: 1px solid #349aff;
    box-shadow: inset 0px 0px 5px #ffffff;
    position: absolute;
    top: -1px;
    left: -1px;
    -moz-transform: scale(0);
    -webkit-transform: scale(0);
    -webkit-animation: pulse 1s ease-out;
    -moz-animation: pulse 1s ease-out;
}



#twitter {
	background: url(./styles/lucid_lime/imageset/wholestromgreycomini.jpg);
	background-position:center; 
	width: 400px;
	position: absolute;
	opacity: 0.69;
	height: 334px;
	font-size:10px;
	z-index: -10;
}

#background {
	background: url(./styles/lucid_lime/imageset/background.png) repeat 5% 5%;
	position: absolute;
	top: 0; left: 0; right: 0; bottom: 0;
	z-index: 100;
	/*
	-webkit-animation-name: STAR-MOVE;
	-webkit-animation-duration: 1000s;
	-webkit-animation-timing-function: linear;
	-webkit-animation-iteration-count: infinite;*/
}

#midground {
	background: url(./styles/lucid_lime/imageset/midground.png) repeat 20% 20%;
	position: absolute;
	top: 0; left: 0; right: 0; bottom: 0;
	z-index: 200;
	/*
	-webkit-animation-name: STAR-MOVE;
	-webkit-animation-duration: 1500s;
	-webkit-animation-timing-function: linear;
	-webkit-animation-iteration-count: infinite;
	*/
}

#foreground {
	background: url(./styles/lucid_lime/imageset/foreground.png) repeat 35% 35%;
	position: absolute;
	top: 0; left: 0; right: 0; bottom: 0;
	z-index: 300;
	/*
	-webkit-animation-name: STAR-MOVE;
	-webkit-animation-duration: 1500s;
	-webkit-animation-timing-function: linear;
	-webkit-animation-iteration-count: infinite;
	*/
}



#page-wrap {
	width: 100%;
	height: 100%;
	margin: 0 0;
	position: relative;
	z-index: 300;
	overflow: hidden;
}


            div,form {
                
	-webkit-box-shadow: 0 12px 7px -6px black;
	   -moz-box-shadow: 0 12px 7px -6px black;
	        box-shadow: 0 12px 7px -6px black;

            }
			
			.border,
			
			/* Layout with mask */
			.rain{
				 padding: 10px 12px 12px 10px;
				 -moz-box-shadow: 10px 10px 10px rgba(0,0,0,1) inset, -9px -9px 8px rgba(0,0,0,1) inset;
				 -webkit-box-shadow: 8px 8px 8px rgba(0,0,0,1) inset, -9px -9px 8px rgba(0,0,0,1) inset;
				 box-shadow: 8px 8px 8px rgba(0,0,0,1) inset, -9px -9px 8px rgba(0,0,0,1) inset;
				 /*margin: 100px auto;*/
			}
			/* Artifical "border" to clear border to bypass mask */
			.border{
				padding: 1px;
				-moz-border-radius: 5px;
			    -webkit-border-radius: 5px;
				border-radius: 5px;
			}

			.border,
			.rain,
			.border.start,
			.rain.start{
				background-repeat: repeat-x, repeat-x, repeat-x, repeat-x;
				background-position: 0 0, 0 0, 0 0, 0 0;
				/* Blue-ish Green Fallback for Mozilla */
				background-image: -moz-linear-gradient(left, #09ba5e 0%, #00c7ce 15%, #3472cf 26%, #00c8cf 48%, #0ccf91 91%, #09ba5e 100%);
				/* Add "Highlight" Texture to the Animation */
				background-image: -webkit-gradient(linear, left top, right top, color-stop(1%,rgb(9,186,94)), color-stop(15%,rgb(0,199,206)), color-stop(26%,rgb(52,144,207)), color-stop(48%,rgb(0,200,207)), color-stop(91%,rgb(12,207,145)), color-stop(100%,rgb(9,186,94)));
				/* Starting Color */
				background-color: #349aff;
				/* Just do something for IE-suck */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00BA1B', endColorstr='#00BA1B',GradientType=1 );
			}

			/* Non-keyframe fallback animation */
			.border.end,
			.rain.end{
			/*
				-moz-transition-property: background-position;  
				-moz-transition-duration: 30s;
				-moz-transition-timing-function: linear;
				
				-webkit-transition-property: background-position;  
				-webkit-transition-duration: 30s;  
				-webkit-transition-timing-function: linear;
				-o-transition-property: background-position;  
				-o-transition-duration: 30s;  
				-o-transition-timing-function: linear;
				transition-property: background-position;  
				transition-duration: 30s;  
				transition-timing-function: linear;
				background-position: -5400px 0, -4600px 0, -3800px 0, -3000px 0;*/	
			}

			/* Keyfram-licious animation 
			@-webkit-keyframes colors {
			    0% {background-color: #349aff;}
			    15% {background-color: #f347cb;}
			    30% {background-color: #4452f3;}
			    45% {background-color: #44f361;}
			    60% {background-color: #f358d4;}
			    75% {background-color: #eef355;}
			    90% {background-color: #F20006;}
			    100% {background-color: #349aff;}
		    }*/
		    
		    @-webkit-keyframes colors {
			    0% {background-color: #2c7f3a;}
			    15% {background-color: #22cb41;}
			    30% {background-color: #4eeb68;}
			    45% {background-color: #13bb32;}
			    60% {background-color: #44f361;}
			    75% {background-color: #14fa3b;}
			    90% {background-color: #19d138;}
			    100% {background-color: #2c7f3a;}
		    }
		    .border,.rain{
		      /*
			    -webkit-animation-direction: normal;
			    -webkit-animation-duration: 20s;
			    -webkit-animation-iteration-count: infinite;
			    -webkit-animation-name: colors;
			    -webkit-animation-timing-function: ease;*/
		    }

		    /* In-Active State Style */
			.border.unfocus{
				background: #333 !important;	
				 -moz-box-shadow: 0px 0px 15px rgba(255,255,255,.2);
				 -webkit-box-shadow: 0px 0px 15px rgba(255,255,255,.2);
				 box-shadow: 0px 0px 15px rgba(255,255,255,.2);
				 -webkit-animation-name: none;
			}
			.rain.unfocus{
				background: #000 !important;	
				-webkit-animation-name: none;
			}

			/* Regular Form Styles */
			form{
				background: #212121;
				-moz-border-radius: 5px;
				-webkit-border-radius: 5px;
			    border-radius: 5px;
				height: 100%;
				width: 100%;
				background: -moz-radial-gradient(50% 46% 90deg,circle closest-corner, #232323, #0a0a0a);
				background: -webkit-gradient(radial, 50% 50%, 0, 50% 50%, 150, from(#242424), to(#090909));
			}
			form label{
				display: block;
				padding: 10px 10px 5px 15px;
				font-size: 11px;
				color: #777;
			}
			form input{
				display: block;
				margin: 5px 10px 10px 15px;
				width: 85%;
				background: #111;
				-moz-box-shadow: 0px 0px 4px #000 inset;
				-webkit-box-shadow: 0px 0px 4px #000 inset;
				box-shadow: 0px 0px 4px #000 inset;
				outline: 1px solid #333;
				border: 1px solid #000;
				padding: 5px;
				color: #444;
				font-size: 16px;
			}
			form input:focus{
				outline: 1px solid #555;
				color: #FFF;
			}
			input[type="submit"]{
				color: #999;
				padding: 5px 10px;
				float: right;
				margin: 20px 0;
				border: 1px solid #000;
				font-weight: lighter;
				-moz-border-radius: 15px;
			    -webkit-border-radius: 15px;
				border-radius: 15px;
				background: #45484d;
				background: -moz-linear-gradient(top, #222 0%, #111 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#222), color-stop(100%,#111));
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#22222', endColorstr='#11111',GradientType=0 );
				-moz-box-shadow: 0px 1px 1px #000, 0px 1px 0px rgba(255,255,255,.3) inset;
				-webkit-box-shadow: 0px 1px 1px #000, 0px 1px 0px rgba(255,255,255,.3) inset;
				box-shadow: 0px 1px 1px #000,0px 1px 0px rgba(255,255,255,.3) inset;
				text-shadow: 0 1px 1px #000;
			}
			
			#background {
    			display: none;
			}
			#midground {
    			display: none;
			}
			#foreground {
    			display: none;
			}
			
		</style>

</head>

<body class="section-<?php echo (isset($this->_rootref['SCRIPT_NAME'])) ? $this->_rootref['SCRIPT_NAME'] : ''; ?> <?php echo (isset($this->_rootref['S_CONTENT_DIRECTION'])) ? $this->_rootref['S_CONTENT_DIRECTION'] : ''; ?>" >

<div id="background"></div>
    <div id="midground"></div>
    <div id="foreground"></div>
	
	<div id="page-wrap">
	
<div class="rain" >
<div class="border start" >
<form >
<label></label>
<div id="page-header" >
		<div class="headerbar" style="background: url(./styles/lucid_lime/imageset/notwholestromgrey.jpg);">
			<div class="inner"><span class="corners-top"><span></span></span>

			<div id="site-description">
				<a href="<?php echo (isset($this->_rootref['U_INDEX'])) ? $this->_rootref['U_INDEX'] : ''; ?>"  id="logo">
				
				<img id="logo-img"src="./styles/lucid_lime/imageset/site_logo.png" width="243" height="104" alt="" title="" onmouseover='logohover()' onmouseleave='logoendhover()'/>
				
				
				</a>
				
				
    				
				
				<div style="margin-top: 31px;">
				
				
				<p style="margin-top: 6px;font-size: 1.2em;"><!--<a style="text-align:right" href="https://bitbucket.org/cisary/entropy.sk">bitbutcket</a>&nbsp;--><i>Bitcoin crowfunding platform</i>
				
				<!--&nbsp;:&nbsp;&nbsp;<img style="text-align:right"  src="http://entropy.sk/my/progress.png"/></a>&nbsp;&nbsp;&nbsp;
				-->
				</p>
				
				</div>
		</div>
		<div >
		
		<div style="height:43px;background-color:rgba(0,0,0,0.27);width:100%;">
			<div class="inner"><span class="corners-top"><span></span></span>
			<ul class="rightside">
				<?php if (! $this->_rootref['S_IS_BOT']) {  ?>

				<li class="icon-logout"><a href="<?php echo (isset($this->_rootref['U_LOGIN_LOGOUT'])) ? $this->_rootref['U_LOGIN_LOGOUT'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_LOGIN_LOGOUT'])) ? $this->_rootref['L_LOGIN_LOGOUT'] : ((isset($user->lang['LOGIN_LOGOUT'])) ? $user->lang['LOGIN_LOGOUT'] : '{ LOGIN_LOGOUT }')); ?>" accesskey="x"><?php echo ((isset($this->_rootref['L_LOGIN_LOGOUT'])) ? $this->_rootref['L_LOGIN_LOGOUT'] : ((isset($user->lang['LOGIN_LOGOUT'])) ? $user->lang['LOGIN_LOGOUT'] : '{ LOGIN_LOGOUT }')); ?></a></li>
					
					<?php if (! $this->_rootref['S_USER_LOGGED_IN'] && $this->_rootref['S_REGISTER_ENABLED'] && ! ( $this->_rootref['S_SHOW_COPPA'] || $this->_rootref['S_REGISTRATION'] )) {  ?><li class="icon-register"><a href="<?php echo (isset($this->_rootref['U_REGISTER'])) ? $this->_rootref['U_REGISTER'] : ''; ?>"><?php echo ((isset($this->_rootref['L_REGISTER'])) ? $this->_rootref['L_REGISTER'] : ((isset($user->lang['REGISTER'])) ? $user->lang['REGISTER'] : '{ REGISTER }')); ?></a></li><?php } } if (! $this->_rootref['S_IS_BOT'] && $this->_rootref['S_USER_LOGGED_IN'] && ! $this->_rootref['U_PRINT_TOPIC']) {  } if ($this->_rootref['U_PRINT_TOPIC']) {  ?>

        <!--
				<li class="rightside"><a href="<?php echo (isset($this->_rootref['U_PRINT_TOPIC'])) ? $this->_rootref['U_PRINT_TOPIC'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_PRINT_TOPIC'])) ? $this->_rootref['L_PRINT_TOPIC'] : ((isset($user->lang['PRINT_TOPIC'])) ? $user->lang['PRINT_TOPIC'] : '{ PRINT_TOPIC }')); ?>" accesskey="p" class="print"></a></li>
				
				<li class="rightside"><a href="<?php echo (isset($this->_rootref['U_EMAIL_TOPIC'])) ? $this->_rootref['U_EMAIL_TOPIC'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_EMAIL_TOPIC'])) ? $this->_rootref['L_EMAIL_TOPIC'] : ((isset($user->lang['EMAIL_TOPIC'])) ? $user->lang['EMAIL_TOPIC'] : '{ EMAIL_TOPIC }')); ?>" class="sendemail"><?php echo ((isset($this->_rootref['L_EMAIL_TOPIC'])) ? $this->_rootref['L_EMAIL_TOPIC'] : ((isset($user->lang['EMAIL_TOPIC'])) ? $user->lang['EMAIL_TOPIC'] : '{ EMAIL_TOPIC }')); ?></a></li>
	-->
<?php } else if (! $this->_rootref['S_IS_BOT'] && $this->_rootref['S_USER_LOGGED_IN']) {  } ?>							
			</ul>
			
			
			
			

			
			</div>
		</div>

	</div>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<?php if (! $this->_rootref['S_IS_BOT'] && $this->_rootref['S_USER_LOGGED_IN']) {  ?>

		  <ul>&nbsp;&nbsp;<li><a href="<?php echo (isset($this->_rootref['U_MEMBERLIST'])) ? $this->_rootref['U_MEMBERLIST'] : ''; ?>" title="<?php echo ((isset($this->_rootref['L_MEMBERLIST_EXPLAIN'])) ? $this->_rootref['L_MEMBERLIST_EXPLAIN'] : ((isset($user->lang['MEMBERLIST_EXPLAIN'])) ? $user->lang['MEMBERLIST_EXPLAIN'] : '{ MEMBERLIST_EXPLAIN }')); ?>"><?php echo ((isset($this->_rootref['L_MEMBERLIST'])) ? $this->_rootref['L_MEMBERLIST'] : ((isset($user->lang['MEMBERLIST'])) ? $user->lang['MEMBERLIST'] : '{ MEMBERLIST }')); ?></a></li><li>
					   <a href="<?php echo (isset($this->_rootref['U_PRIVATEMSGS'])) ? $this->_rootref['U_PRIVATEMSGS'] : ''; ?>" title="<?php echo (isset($this->_rootref['U_PRIVATEMSGS'])) ? $this->_rootref['U_PRIVATEMSGS'] : ''; ?>" accesskey="e">Inbox</a>
				(<a href="<?php echo (isset($this->_rootref['U_PRIVATEMSGS'])) ? $this->_rootref['U_PRIVATEMSGS'] : ''; ?>"><?php echo (isset($this->_rootref['PRIVATE_MESSAGE_INFO'])) ? $this->_rootref['PRIVATE_MESSAGE_INFO'] : ''; ?></a>)
				&bull;
					<a href="<?php echo (isset($this->_rootref['U_SEARCH_SELF'])) ? $this->_rootref['U_SEARCH_SELF'] : ''; ?>">Posts</a>
					<?php if ($this->_rootref['S_DISPLAY_SEARCH']) {  ?> 
					&bull;&nbsp;&nbsp;<a href="<?php echo (isset($this->_rootref['U_SEARCH_SELF'])) ? $this->_rootref['U_SEARCH_SELF'] : ''; ?>">Search</a>
					&nbsp;&nbsp;&nbsp;
					<?php } if ($this->_rootref['U_RESTORE_PERMISSIONS']) {  ?> &bull;
					<a href="<?php echo (isset($this->_rootref['U_RESTORE_PERMISSIONS'])) ? $this->_rootref['U_RESTORE_PERMISSIONS'] : ''; ?>"><?php echo ((isset($this->_rootref['L_RESTORE_PERMISSIONS'])) ? $this->_rootref['L_RESTORE_PERMISSIONS'] : ((isset($user->lang['RESTORE_PERMISSIONS'])) ? $user->lang['RESTORE_PERMISSIONS'] : '{ RESTORE_PERMISSIONS }')); ?></a>
					<?php } ?>

					</li>
				</ul>
			<?php } else { ?>

			<!--&nbsp; popis nad loginom-->
			<?php } if ($this->_rootref['S_DISPLAY_SEARCH'] && ! $this->_rootref['S_IN_SEARCH']) {  if ($this->_rootref['U_PRINT_TOPIC']) {  ?>

			
			<a onmouseover='twitter2hover()' onmouseleave='twitter2endhover()' style="text-align:right" href="https://twitter.com"><img id="twitter2-img" style="text-align:right"  src="http://entropy.sk/my/twitter_h.png" onmouseover='twitter2hover()' onmouseleave='twitter2endhover()'/></a>
				
			<?php } else { ?>	
			<!--
			<a onmouseover='trellohover()' onmouseleave='trelloendhover()' style="text-align:right" href="https://twitter.com">
			     
			     <img id="trello-img" style="text-align:right"  src="http://entropy.sk/my/trello_h.png" onmouseover='trellohover()' onmouseleave='trelloendhover()'/>
			     
			     </a>-->
			<!--
			     <a onmouseover='twitterhover()' onmouseleave='twitterendhover()' style="text-align:right" href="https://twitter.com"><img id="twitter-img" style="text-align:right"  src="http://entropy.sk/my/twitter_h.png" onmouseover='twitterhover()' onmouseleave='twitterendhover()'/></a>
			 -->
			     <?php if (! $this->_rootref['S_IS_BOT'] && ! $this->_rootref['S_USER_LOGGED_IN']) {  } } ?>	
			</div>
			
		<?php } ?>

		
</div>

		
</div>

<div>
	<a id="bottom" name="bottom" accesskey="z"></a>
	<?php if (! $this->_rootref['S_IS_BOT']) {  echo (isset($this->_rootref['RUN_CRON_TASK'])) ? $this->_rootref['RUN_CRON_TASK'] : ''; } ?>

</div>



<div id="wrap" >
	<a id="top" name="top" accesskey="t"></a>
	


		

	<?php if (! $this->_rootref['S_IS_BOT'] && $this->_rootref['S_USER_LOGGED_IN'] && $this->_rootref['S_DISPLAY_PM']) {  if ($this->_rootref['S_USER_NEW_PRIVMSG']) {  ?>

			<div class="pm_alert">
            	<h3><?php echo ((isset($this->_rootref['L_PRIVATE_MESSAGES'])) ? $this->_rootref['L_PRIVATE_MESSAGES'] : ((isset($user->lang['PRIVATE_MESSAGES'])) ? $user->lang['PRIVATE_MESSAGES'] : '{ PRIVATE_MESSAGES }')); ?></h3>
                <div style="margin-bottom:5px !important;"><a href="<?php echo (isset($this->_rootref['U_PRIVATEMSGS'])) ? $this->_rootref['U_PRIVATEMSGS'] : ''; ?>"><?php echo (isset($this->_rootref['PRIVATE_MESSAGE_INFO'])) ? $this->_rootref['PRIVATE_MESSAGE_INFO'] : ''; if ($this->_rootref['PRIVATE_MESSAGE_INFO_UNREAD']) {  ?>, <?php echo (isset($this->_rootref['PRIVATE_MESSAGE_INFO_UNREAD'])) ? $this->_rootref['PRIVATE_MESSAGE_INFO_UNREAD'] : ''; } ?></a></div>
            </div>
        <?php } else if ($this->_rootref['S_USER_UNREAD_PRIVMSG']) {  ?>

        	<div class="pm_alert">
            	<h3><?php echo ((isset($this->_rootref['L_PRIVATE_MESSAGES'])) ? $this->_rootref['L_PRIVATE_MESSAGES'] : ((isset($user->lang['PRIVATE_MESSAGES'])) ? $user->lang['PRIVATE_MESSAGES'] : '{ PRIVATE_MESSAGES }')); ?></h3>
                <div style="margin-bottom:5px !important;"><a href="<?php echo (isset($this->_rootref['U_PRIVATEMSGS'])) ? $this->_rootref['U_PRIVATEMSGS'] : ''; ?>"><?php echo (isset($this->_rootref['PRIVATE_MESSAGE_INFO'])) ? $this->_rootref['PRIVATE_MESSAGE_INFO'] : ''; if ($this->_rootref['PRIVATE_MESSAGE_INFO_UNREAD']) {  ?> &bull; <?php echo (isset($this->_rootref['PRIVATE_MESSAGE_INFO_UNREAD'])) ? $this->_rootref['PRIVATE_MESSAGE_INFO_UNREAD'] : ''; } ?></a></div>
            
		<?php } } ?>


	<a name="start_here"></a>
	<div id="page-body">
		<?php if ($this->_rootref['S_BOARD_DISABLED'] && $this->_rootref['S_USER_LOGGED_IN'] && ( $this->_rootref['U_MCP'] || $this->_rootref['U_ACP'] )) {  ?>

		<div id="information" class="rules">
			<div class="inner"><span class="corners-top"><span></span></span>
				<strong><?php echo ((isset($this->_rootref['L_INFORMATION'])) ? $this->_rootref['L_INFORMATION'] : ((isset($user->lang['INFORMATION'])) ? $user->lang['INFORMATION'] : '{ INFORMATION }')); ?>:</strong> <?php echo ((isset($this->_rootref['L_BOARD_DISABLED'])) ? $this->_rootref['L_BOARD_DISABLED'] : ((isset($user->lang['BOARD_DISABLED'])) ? $user->lang['BOARD_DISABLED'] : '{ BOARD_DISABLED }')); ?>

			<span class="corners-bottom"><span></span></span></div>
		</div>
		<?php } ?>

		
</form>
</div>
</div>